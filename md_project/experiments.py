import numpy
import matplotlib.pyplot as plt
import constants
import dataset
import itertools
from sklearn.metrics import confusion_matrix, precision_recall_fscore_support
from sklearn.metrics import f1_score
from sklearn.metrics import accuracy_score
from imageLoader import load_data_set
from predict import predict


def plot_confusion_matrix(cm, classes,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, numpy.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = numpy.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_xticklabels([''] + constants.class_labels)
    ax.set_yticklabels([''] + constants.class_labels)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    plt.show()


[ds, no, testing_set] = load_data_set()

testing_data_set = dataset.read_train_sets().test

y_actual = testing_data_set.labels
y_predicted = []


def decoder(x):
    return list(x).index(1)


index = decoder([0, 0, 1])

y_actual = [decoder(x) for x in y_actual]

for testing_image in testing_data_set.images:
    predicted = numpy.array(predict(testing_image))
    index_of_maximum = numpy.argmax(predicted)
    predicted = numpy.zeros(len(constants.class_labels))
    predicted[index_of_maximum] = 1
    y_predicted.append(decoder(predicted))

cm = confusion_matrix(y_actual, y_predicted)
accuracy = accuracy_score(y_actual, y_predicted)
f1 = f1_score(y_actual, y_predicted, average="micro")

print "F score: "
print f1
print "Accuracy: "
print accuracy

plot_confusion_matrix(cm, constants.class_labels, normalize=True)
