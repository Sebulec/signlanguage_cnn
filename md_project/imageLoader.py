import random

import os
import numpy as np
from itertools import islice
import glob

import sklearn
from PIL import Image
from sklearn.model_selection import train_test_split

size = 128, 128


def load_data_set():
    all_images_with_classes = get_all_images()
    print "Found : ", all_images_with_classes.values().__len__(), " images"
    all_classes = set(all_images_with_classes.values())
    all_classes = list(all_classes)
    x = all_images_with_classes.keys()
    y = map(lambda x: all_classes.index(x), all_images_with_classes.values())

    x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.2, random_state=1)

    x_train, x_val, y_train, y_val = train_test_split(x_train, y_train, test_size=0.2, random_state=1)

    return [x_train, y_train, x_val, y_val, x_test, y_test]


def get_all_images():
    images_with_classes = {}
    img_files = [os.path.join(root, name)
                 for root, dirs, files in os.walk('thumbnails')
                 for name in files
                 if name.endswith(".png")]
    np.random.shuffle(img_files)
    for imgFile in img_files:
        class_label = imgFile.split("-")[0].upper().split("/")[1]
        images_with_classes[imgFile] = class_label
    return images_with_classes


def convert_images():
    img_files = [os.path.join(root, name)
                 for root, dirs, files in os.walk('dataset5')
                 for name in files
                 if name.endswith(".png") and "depth" not in name]
    convert_images_to_constant_size(img_files)


def convert_images_to_constant_size(images):
    relative_path = "/Developer/MD/Projekt/md_project/thumbnails/"
    i = 0
    for image in images:
        img = Image.open(image)
        img = img.resize(size, Image.ANTIALIAS)
        dir = os.path.dirname(image)
        class_name = dir.split("/")[2].upper()
        image_name = image.split("/")[3]
        print "image: ", i
        i = i + 1
        if not os.path.exists(relative_path + class_name):
            os.makedirs(relative_path + class_name)
        img.save(relative_path + class_name + "/" + class_name + "-" + str(i) + image_name)


def move_files():
    img_files = [os.path.join(root, name)
                 for root, dirs, files in os.walk('thumbnails')
                 for name in files
                 if name.endswith(".jpg") and "depth" not in name]
    # img_files = glob.glob("thumbnails/*.jpg")
    relative_path = "/Developer/MD/Projekt/md_project/thumbnails2/"
    i = 0
    for image in img_files:
        img = Image.open(image)
        img = img.resize(size, Image.ANTIALIAS)
        dir = os.path.dirname(image)
        class_name = image.split(".")[0].upper()[-1:]
        print "image: ", i
        i = i + 1
        if not os.path.exists(relative_path + class_name):
            os.makedirs(relative_path + class_name)
        img.save(relative_path + class_name + "/" + class_name + "-" + str(i) + ".jpg")
