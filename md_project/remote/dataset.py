from random import shuffle
from cv2 import cv2
import numpy as np

from imageLoader import load_data_set


def load_train(files, labels):
    images = []
    img_names = []
    # relative_path = "~/dataset/thumbnails/"
    relative_path = "/Developer/MD/Projekt/md_project/"

    print('Going to read images')
    # for fields in classes:
    #     index = classes.index(fields)
    #     print('Now going to read {} files (Index: {})'.format(fields, index))
    i = 0
    for filename in files:
        image = cv2.imread(relative_path + filename)
        image = image.astype(np.float32)
        image = np.multiply(image, 1.0 / 255.0)
        images.append(image)
        img_names.append(filename)
        print i
        i += 1
    images = np.array(images)
    labels = np.array(labels)
    img_names = np.array(img_names)

    return images, labels, img_names


class DataSet(object):
    def __init__(self, images, labels, img_names):
        self._num_examples = images.shape[0]
        self._images = images
        self._labels = labels
        self._img_names = img_names
        self._epochs_done = 0
        self._index_in_epoch = 0

    @property
    def images(self):
        return self._images

    @property
    def labels(self):
        return self._labels

    @property
    def img_names(self):
        return self._img_names

    @property
    def num_examples(self):
        return self._num_examples

    @property
    def epochs_done(self):
        return self._epochs_done

    def next_batch(self, batch_size):
        """Return the next `batch_size` examples from this data set."""
        start = self._index_in_epoch
        self._index_in_epoch += batch_size

        if self._index_in_epoch > self._num_examples:
            # After each epoch we update this
            self._epochs_done += 1
            start = 0
            self._index_in_epoch = batch_size
            assert batch_size <= self._num_examples
        end = self._index_in_epoch

        return self._images[start:end], self._labels[start:end], self._img_names[start:end]


def read_train_sets():
    class DataSets(object):
        pass

    data_sets = DataSets()

    [x_train, y_train, x_val, y_val, x_test, y_test] = load_data_set()

    [train_images, train_labels, train_img_names] = load_train(x_train, y_train)
    [validation_images, validation_labels, validation_img_names] = load_train(x_val, y_val)
    [test_images, test_labels, test_img_names] = load_train(x_test, y_test)

    data_sets.train = DataSet(train_images, train_labels, train_img_names)
    data_sets.valid = DataSet(validation_images, validation_labels, validation_img_names)
    data_sets.test = DataSet(test_images, test_labels, test_img_names)

    return data_sets
